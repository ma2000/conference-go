from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from events.models import Conference
from .models import Attendee


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        a = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
        {"attendees": a},
        encoder=AttendeeListEncoder,
    )
    
    else:
        y = json.loads(request.body)

        try:
            c = Conference.objects.get(id=conference_id)
            y["conference"] = c
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference id"},
                status=400,
            )

        z = Attendee.objects.create(**y)

        return JsonResponse(
            z,
            encoder=AttendeeDetailEncoder,
            safe =False
        )


    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    # List_Attendees = [
    #         {
    #             "name": x.name,
    #             "href": x.get_api_url(),
    #         }
    #         for x in Attendee.objects.filter(conference=conference_id)
    # ]

    # return JsonResponse({"attendees": List_Attendees})

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        individual_attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            individual_attendee,
            encoder=AttendeeDetailEncoder,
            safe=False
        )
    
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })

    else:
        y = json.loads(request.body)
        update_attendee = Attendee.objects.filter(id=pk).update(**y)
        return JsonResponse(
            update_attendee,
            encoder=AttendeeDetailEncoder,
            safe=False
        )



    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # y = Attendee.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "email": y.email,
    #         "name": y.name,
    #         "company_name": y.company_name,
    #         "created": y.created,
    #         "conference": {
    #             "name": y.conference.name,
    #             "href": y.conference.get_api_url(),
    #         },
    #     }
    # )
