from django.http import JsonResponse

from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",  
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return { "state": o.state.abbreviation,
                "picture_url": get_photo(o.state.name, o.city)}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        y = json.loads(request.body)
        try:
            get_location = Location.objects.get(id=y["location"])
            y["location"] = get_location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        create_c = Conference.objects.create(**y)
        return JsonResponse(
            create_c,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


    # conferences = Conference.objects.all()
    # return JsonResponse(
    #     {"conferences": conferences},
    #     encoder=ConferenceListEncoder,
    # )

    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, pk):
    if request.method == "GET":
        individual_c = Conference.objects.get(id=pk)
        
        C = individual_c.location.city
        S= individual_c.location.state.name
        W = get_weather_data(C, S)

        return JsonResponse(
            # individual_c,
            {"conference": individual_c, "weather": W},
            encoder=ConferenceDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        try:
            y = json.loads(request.body)
            individual_location = Location.objects.get(id=y["location"])
            y["location"] = individual_location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid id"},
                status=400,
            )

        update_conferences = Conference.objects.filter(id=pk).update(**y)
        return JsonResponse(
            update_conferences,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


    """
    Returns the details for the Conference model specified
    by the pk parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    # conference = Conference.objects.get(id=pk)
    # return JsonResponse(
    #     conference,
    #     encoder=ConferenceDetailEncoder,
    #     safe=False
    # )

    # conference = Conference.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        all_locations = Location.objects.all()
        return JsonResponse(
        {"locations": all_locations},
        encoder=LocationListEncoder,
        )

    else:
        y = json.loads(request.body)
        try:
            all = State.objects.get(abbreviation=y["state"])
            y["state"] = all
            photo = get_photo(y["city"], all.name)
            y.update(photo)

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400
            )
        create_location = Location.objects.create(**y)
        return JsonResponse(
            create_location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    # List_Locations = [
    #     {
    #         "name": x.name,
    #         "href": x.get_api_url()
    #     }
    #     for x in Location.objects.all()
    # ]

    # Location.objects.all()
    # for x in locations:
    #     response.append(
    #         {
    #             "name": x.name,
    #             "href": x.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"locations": List_Locations})

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    if request.method == "GET":
        get_location = Location.objects.get(id=pk)

        return JsonResponse(
        get_location,
        encoder=LocationDetailEncoder,
        safe=False,
    )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=pk).update(**content)
        get_location = Location.objects.get(id=pk)
        return JsonResponse(
            get_location,
            encoder=LocationDetailEncoder,
            safe=False
        )


    """
    Returns the details for the Location model specified
    by the pk parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    # y = Location.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "name": y.name,
    #         "city": y.city,
    #         "room_count": y.room_count,
    #         "created": y.created,
    #         "updated": y.updated,
    #         "state": y.state.abbreviation,
    #     }
    # )
