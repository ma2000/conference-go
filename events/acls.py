from tkinter import Y
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json, requests



def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    x = requests.get(url, params=params, headers=headers)
    content = json.loads(x.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}




def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # url = "http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}"
    
    params = {"q": f"{city},{state},US", "limit": 1, "appid": OPEN_WEATHER_API_KEY}
    x = requests.get(url, params=params)
    y = json.loads(x.content)

    L1 = y[0]["lat"]
    L2 = y[0]["lon"]
    params = {"lat": L1, "lon": L2, "units": "imperial", "appid": OPEN_WEATHER_API_KEY}
    url = "https://api.openweathermap.org/data/2.5/weather"
    x = requests.get(url, params=params)
    content = json.loads(x.content)

    try:
        return{"weather": content["weather"], "temp": content["main"]["temp"] }
    except:
        return {"weather": None}

   
    

#   url = "http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}
