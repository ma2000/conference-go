from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Presentation
from common.json import ModelEncoder
import json


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

@require_http_methods(["GET", "DELETE", "PUT"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        all = Presentation.objects.all()
        return JsonResponse(
            all,
            encoder=PresentationDetailEncoder,
            safe=False
        )
    
    if request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })

    if request.method == "PUT":
        y = json.loads(request.body)
        filtered_updates = Presentation.objects.filter(id=pk).update(**y)
        return JsonResponse(
            filtered_updates,
            encoder=PresentationDetailEncoder,
            safe=False
        )


    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    # List_Presentations = [
    #     {
    #         "title": x.title,
    #         "status": x.status.name,
    #         "href": x.get_api_url(),
    #     }
    #     for x in Presentation.objects.all()
    # ]

    # return JsonResponse({"presentations": List_Presentations})


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        x = Presentation.objects.get(id=pk)
        return JsonResponse(
            x,
            encoder=PresentationDetailEncoder,
            safe=False
        )

    if request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse({
            "deleted": count > 0
        })

    if request.method == "PUT":
        y = json.loads(request.body)
        z = Presentation.objects.filter(id=pk).update(**y)
        return JsonResponse(
            z,
            encoder=PresentationDetailEncoder,
            safe=False
        )



    # presentation = Presentation.objects.get(id=pk)
    # return JsonResponse(
    #     presentation,
    #     encoder = PresentationDetailEncoder,
    #     safe=False,
    # )
    


    """
    Returns the details for the Presentation model specified
    by the pk parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # y = Presentation.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "presenter_name": y.presenter_name,
    #         "company_name": y.company_name,
    #         "presenter_email": y.presenter_email,
    #         "title": y.title,
    #         "synopsis": y.synopsis,
    #         "created": y.created,
    #         "status": y.status.name,
    #         "conference": {
    #             "name": y.conference.name,
    #             "href": y.conference.get_api_url(),
    #         },
    #     }
    # )

